//
//  ViewController.swift
//  XFYWebView
//
//  Created by leonazhu on 03/21/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYWebView

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toWebViewController(_ sender: Any) {
        let webViewController = WebViewController(url: "https://www.baidu.com")
        navigationController?.pushViewController(webViewController, animated: true)
    }
}
