# XFYWebView

[![CI Status](https://img.shields.io/travis/leonazhu/XFYWebView.svg?style=flat)](https://travis-ci.org/leonazhu/XFYWebView)
[![Version](https://img.shields.io/cocoapods/v/XFYWebView.svg?style=flat)](https://cocoapods.org/pods/XFYWebView)
[![License](https://img.shields.io/cocoapods/l/XFYWebView.svg?style=flat)](https://cocoapods.org/pods/XFYWebView)
[![Platform](https://img.shields.io/cocoapods/p/XFYWebView.svg?style=flat)](https://cocoapods.org/pods/XFYWebView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYWebView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYWebView'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYWebView is available under the MIT license. See the LICENSE file for more info.
