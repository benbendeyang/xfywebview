//
//  WebViewController.swift
//  Pods-XFYWebView_Example
//
//  Created by 🐑 on 2019/3/21.
//

import UIKit
import XFYBaseController
import WebKit

open class WebViewController: BaseViewController {
    
    private static let TitleKey: String = "title"
    private static let ProgressKey: String = "estimatedProgress"
    
    private let webview = WKWebView()
    private let progressView = UIProgressView(progressViewStyle: .bar)
    private var url: String
    
    public init(url: String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initController()
    }
    
    deinit {
        webview.stopLoading()
        webview.removeObserver(self, forKeyPath: WebViewController.TitleKey)
        webview.removeObserver(self, forKeyPath: WebViewController.ProgressKey)
        webview.navigationDelegate = nil
    }
}

// MARK: - 私有方法
private extension WebViewController {
    
    func initController() {
        webview.navigationDelegate = self
        webview.addObserver(self, forKeyPath: WebViewController.TitleKey, options: .new, context: nil)
        webview.addObserver(self, forKeyPath: WebViewController.ProgressKey, options: .new, context: nil)
        load()
        view.addSubview(webview)
        webview.translatesAutoresizingMaskIntoConstraints = false
        let webviewLeft = webview.leftAnchor.constraint(equalTo: view.leftAnchor)
        let webviewRight = webview.rightAnchor.constraint(equalTo: view.rightAnchor)
        let webviewTop = webview.topAnchor.constraint(equalTo: view.topAnchor)
        let webviewBottom = webview.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        NSLayoutConstraint.activate([webviewLeft, webviewRight, webviewTop, webviewBottom])
        
        view.addSubview(progressView)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        let progressLeft = progressView.leftAnchor.constraint(equalTo: view.leftAnchor)
        let progressRight = progressView.rightAnchor.constraint(equalTo: view.rightAnchor)
        var progressTop: NSLayoutConstraint!
        if #available(iOS 11.0, *) {
            progressTop = progressView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        } else {
            progressTop = progressView.topAnchor.constraint(equalTo: view.topAnchor)
        }
        let progressHeight = progressView.heightAnchor.constraint(equalToConstant: 3)
        NSLayoutConstraint.activate([progressLeft, progressRight, progressTop, progressHeight])
    }
    
    func load() {
        guard let webviewUrl = URL(string: url) else { return }
        webview.load(URLRequest(url: webviewUrl))
    }
    
    func hiddenProgressView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.progressView.alpha = 0
        }) { _ in
            self.progressView.progress = 0
        }
    }
}

// MARK: - 协议
extension WebViewController: WKNavigationDelegate {
    
    private func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        // 开始加载
        progressView.alpha = 1
        progressView.setProgress(0.1, animated: true)
    }
    
    private func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // 加载完成
    }
    
    private func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        // 加载失败
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == WebViewController.TitleKey {
            title = webview.title
        } else if keyPath == WebViewController.ProgressKey {
            if webview.estimatedProgress > 0.1 {
                progressView.setProgress(Float(webview.estimatedProgress), animated: true)
                if webview.estimatedProgress == 1 {
                    hiddenProgressView()
                }
            }
        }
    }
}
